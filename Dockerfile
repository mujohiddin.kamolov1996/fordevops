FROM python:3.10.12-alpine

RUN pip install --upgrade pip

COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY ./mysite /app

WORKDIR /app

COPY ./entrypoint /
ENTRYPOINT ["sh", "/entrypoint"]